﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    [SerializeField]
    private float rocketBodyLength = 0;

    

    // Start is called before the first frame update
    void Start()
    {
        InitRocket();
        ObjectRelativeToTerrainAdjuster.SetObjectToBeginOfTerrain(this.gameObject);
        ObjectRelativeToTerrainAdjuster.SetObjectOnTopOfTerrain(this.gameObject,GetComponentsInChildren<Transform>());
        
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void InitRocket()
    {
        RocketBodySetup();
        RocketBoostersSetup();
    
    }

    private void RocketBodySetup()
    {
        GameObject rocketBody = gameObject.transform.Find("RocketBody").gameObject;
        Vector3 scale = rocketBody.transform.localScale;
        scale.y = rocketBodyLength;

        rocketBody.transform.localScale = scale;
    }
    private void RocketBoostersSetup()
    {
        GameObject leftRocketBooster = GetRocketPartGameObject("LeftRocketBooster");
        GameObject rightRocketBooster = GetRocketPartGameObject("RightRocketBooster");

        Vector3 leftPosition = leftRocketBooster.transform.position;
        Vector3 rightPosition = rightRocketBooster.transform.position;
        Vector3 rocketBodyPosition = GetRocketPartGameObject("RocketBody").transform.position;
        float radius = GetRocketPartGameObject("RocketBody").GetComponent<SphereCollider>().radius;

        leftPosition.y =- ( (rocketBodyLength / 2) *  1.2f); 
        Vector3 distanceTest = leftPosition - rocketBodyPosition;
      
        Vector3 radiusPosition = new Vector3(distanceTest.normalized.x + radius, distanceTest.normalized.y + radius, distanceTest.normalized.z + radius);

        Vector2 unitX = new Vector2(1,0);
        Vector2 unitRadius = new Vector2(radiusPosition.normalized.x,radiusPosition.normalized.y);
    
        float angle = Mathf.Acos(Vector2.Dot(unitX,unitRadius));

        Debug.Log($"ovo je angle  {angle}");
        
        float sinus = Mathf.Sin(angle);
        float distance = 2 * (sinus * radius);

        leftPosition.x = rocketBodyPosition.x + (-distance / 2);
        rightPosition.x = rocketBodyPosition.x + (distance / 2);

     
        Debug.Log($"Ovo je length {rocketBodyLength}");

        leftRocketBooster.transform.position = leftPosition;
        rightRocketBooster.transform.position = rightPosition;
       
    }


    private GameObject GetRocketPartGameObject(string partName)
    {
        return gameObject.transform.Find(partName).gameObject;
    }
}
