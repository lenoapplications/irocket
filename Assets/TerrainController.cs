﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TerrainController : MonoBehaviour
{
    

    [SerializeField] private float terrainWidth;

    public float TerrainWidth
    {
        get{return terrainWidth;}   
    }
    
    // Start is called before the first frame update
    void Start()
    {
      
        setupTerrain();
    }

    // Update is called once per frame
    void Update()
    {
        
    }




    //Method for setting up a terrain before game starts
    private void setupTerrain()
    {
        GameObject camera = GameObject.Find("Main Camera");        
        transform.localScale = new Vector3(terrainWidth, 8, 10);
        CameraAdjuster.InitialAdjustmentOfCamera(camera, this.gameObject);
    }







}
