using System.Collections;
using System.Collections.Generic;
using UnityEngine;



class CameraAdjuster
{
    private static readonly CameraAdjuster cameraAdjuster = new CameraAdjuster();

    public static void InitialAdjustmentOfCamera(GameObject camera, GameObject terrain) {
        cameraAdjuster.AdjustCameraRelativeToTerrain(camera.transform, terrain.transform);
    }

    private void AdjustCameraRelativeToTerrain(Transform cameraTransform, Transform terrainTransform)
    {
        float terrainZPosition = (terrainTransform.localScale.z / 2);
        Vector3 terrainOuterRightSidePosition = (terrainTransform.position + new Vector3(0, 0, terrainZPosition)) * -1;
        Vector3 terrainOuterLeftSidePosition = (terrainTransform.position + new Vector3(0, 0, terrainZPosition));

        CalculateCameraHorizontalPosition(cameraTransform,terrainTransform,terrainOuterRightSidePosition);
        CalculateCameraHeight(cameraTransform, terrainTransform ,terrainOuterRightSidePosition);
        CalculateCameraAngle(cameraTransform, terrainOuterRightSidePosition);
    }

    private void CalculateCameraAngle(Transform cameraTransform, Vector3 terrainOuterRightSidePosition)
    {
        Vector2 unitZ = new Vector2(-1, 0);
        Vector2 cameraVector2 = new Vector2(cameraTransform.position.z,cameraTransform.position.y);
        Vector2 terrainOuterRightSideVector2 = new Vector2(terrainOuterRightSidePosition.z,terrainOuterRightSidePosition.y);

        Vector2 vectorBetweenCameraAndRightSidePosition = cameraVector2 - terrainOuterRightSideVector2;
        float dotProduct = Vector2.Dot(vectorBetweenCameraAndRightSidePosition.normalized, unitZ);
        float angle = Mathf.Acos(dotProduct) * Mathf.Rad2Deg;

        cameraTransform.eulerAngles = new Vector3(angle * 0.5f , 0, 0);
    }
    private void CalculateCameraHeight(Transform cameraTransform, Transform terrainTransform ,Vector3 terrainOuterRightSidePosition)
    {
        float difference = Mathf.Abs(cameraTransform.position.y - terrainOuterRightSidePosition.y);
        Vector3 cameraPosition = cameraTransform.position;

     
        cameraTransform.position = cameraPosition;
    }

    private void CalculateCameraHorizontalPosition(Transform cameraTransform,Transform terrainTransform,Vector3 terrainOuterRightSidePosition)
    {
        float zDistance = Mathf.Abs(cameraTransform.position.z - terrainOuterRightSidePosition.z );
        float bottomAngle = (90 - (Camera.main.fieldOfView / 2)) * Mathf.Deg2Rad;
        float sin = Mathf.Sin(bottomAngle);
        float hypotenuses = zDistance / sin;
        float halfWidth = Mathf.Cos(bottomAngle) * hypotenuses;

        float terrainXPosition = terrainTransform.position.x;
        Vector3 cameraPosition = cameraTransform.position;
        
        terrainXPosition += -terrainTransform.localScale.x /2;
        cameraPosition.x = terrainXPosition + (halfWidth * 2) -7;

        cameraTransform.position = cameraPosition;
    }
 


   

    
   


}