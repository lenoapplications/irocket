using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class ObjectRelativeToTerrainAdjuster
{
    private static readonly ObjectRelativeToTerrainAdjuster objectRelativeToTerrainAdjuster = new ObjectRelativeToTerrainAdjuster();


    private readonly TerrainController terrainController;

    private ObjectRelativeToTerrainAdjuster()
    {
        this.terrainController = (TerrainController) GameObject.Find("Terrain").GetComponent<MonoBehaviour>();
    }


    public static void SetObjectToBeginOfTerrain(GameObject objectToSet)
    {
        Vector3 terrainPosition = objectRelativeToTerrainAdjuster.GetTerrainBeginPosition();
        Vector3 objectPosition = objectToSet.transform.position;
        objectPosition.x = terrainPosition.x;
        
        objectToSet.transform.position = objectPosition;
    }

    public static void SetObjectOnTopOfTerrain(GameObject parentObject,Transform[] objectChildren)
    {
        Vector3 parentPosition = parentObject.transform.position;
        Vector3 terrainPosition = objectRelativeToTerrainAdjuster.GetTerrainBeginPosition();
        float lowestPoint = objectRelativeToTerrainAdjuster.FindGameObjectLowestHeightPosition(objectChildren);

        parentPosition.y +=Mathf.Abs(terrainPosition.y - lowestPoint);
        parentObject.transform.position = parentPosition;
    }


    public static void Test(GameObject parentObject)
    {
        GameObject leftRocket = parentObject.transform.Find("LeftRocketBooster").gameObject;
        Vector3 position = leftRocket.transform.position;
        SphereCollider sphere = parentObject.transform.Find("RocketBody").GetComponent<SphereCollider>();
        float xPosition = parentObject.transform.Find("RocketBody").transform.position.x;
        float radius = sphere.radius;
        float angle = 3 / 2;
        float sinus = Mathf.Sin(angle);

        float distance = 2 * (sinus * radius);
        
        position.x = xPosition + ( - (distance / 2));

        leftRocket.transform.position = position;
    
    }



    private Vector3 GetTerrainBeginPosition()
    {
        Vector3 position = terrainController.transform.position;
        position.x += -terrainController.TerrainWidth / 2;
        position.y += terrainController.transform.localScale.y / 2;
        return position;
    }

    private float FindGameObjectLowestHeightPosition(Transform[] transforms)
    {
        float lowest = transforms[0].position.y;
        foreach(Transform transform in transforms){
            float y =transform.position.y;
            float scale = transform.localScale.y / 2;
            float low = y - scale;
        
            if(low < lowest)
                lowest = low;
            
        }
        return lowest;
    }



    

}